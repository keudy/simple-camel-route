package com.vizuri.demo.camel.demo;

/**
 * An interface for implementing Hello services.
 */
public interface Hello {

    String hello();
	
}
